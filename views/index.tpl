<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>PiPhone Control Panel</title>
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="static/css/bootstrap.min.css" rel="stylesheet">
        <link href="static/css/styles.css" rel="stylesheet">
    </head>
    <body>
        <div id="top-nav" class="navbar navbar-inverse navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-toggle"></span>
                    </button>
                    <a class="navbar-brand" href="#">PiPhone Control Panel</a>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <a href="#"><strong><i class="glyphicon glyphicon-list-alt"></i> Users</strong></a>
                    <hr>
                </div>
                <div class="col-md-8">
                    <form class="form" action="admin" method="post">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>API</th>
                                    <th>Token</th>
                                    <th>admin</th>
                                    <th>delete?</th>
                                </tr>
                            </thead>
                            <tbody>
                                % for user in users:
                                <tr>
                                    <td>{{user[0]}}</td>
                                    <td>{{user[1]}}</td>
                                    <td>{{user[2]}}</td>
                                    <td><input type="checkbox" name="api" value="{{ user[0] }}"></td>
                                </tr>
                                %end
                            </tbody>
                        </table>
                        <button name="action" value="delete" type="submit" class="btn btn-danger pull-right">
                            Delete
                        </button>
                    </form>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h4>Add a user</h4>
                            </div>
                        </div>
                        <div class="panel-body">
                            <form name="action2" class="form form-vertical" action="admin" method="post">
                                <div class="control-group">
                                    <label>API</label>
                                    <div class="controls">
                                        <input name="api" type="text" class="form-control" placeholder="API">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label>Token</label>
                                    <div class="controls">
                                        <input name="api_token" type="text" class="form-control" placeholder="Token">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label>Admin</label>
                                    <div class="controls">
                                        <input name="admin" type="text" class="form-control" placeholder="admin">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label></label>
                                    <div class="controls">
                                        <div class="btn-group pull-right" role="group">
                                            <button class="btn btn-warning" name="action" value="update" type="submit">Update</button>
                                            <button class="btn btn-success" name="action" value="add" type="submit">Add</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <a href="#"><strong><i class="glyphicon glyphicon-list-alt"></i> Blacklisted patterns</strong></a>
                    <hr>
                </div>
                <div class="col-md-8">
                    <form class="form" name="action3" action="admin" method="post">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Pattern</th>
                                    <th>reason</th>
                                    <th>whitelist?</th>
                                </tr>
                            </thead>
                            <tbody>
                                % for blacklist in blacklists:
                                <tr>
                                    <td>{{blacklist[0]}}</td>
                                    <td>{{blacklist[1]}}</td>
                                    <td><input type="checkbox" name="whitelist" value="{{ user[0] }}"></td>
                                </tr>
                                %end
                            </tbody>
                        </table>
                        <button name="action" value="delete" type="submit" class="btn btn-danger pull-right">
                            Whitelist
                        </button>
                    </form>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h4>Blacklist a pattern</h4>
                                <h6>The pattern must match the beginning of a number</h6>
                            </div>
                        </div>
                        <div class="panel-body">
                            <form name="action4" class="form form-vertical" action="admin" method="post">
                                <div class="control-group">
                                    <label>Pattern</label>
                                    <div class="controls">
                                        <input name="pattern" type="text" class="form-control" placeholder="Patter to blacklist">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label>Reason</label>
                                    <div class="controls">
                                        <input name="reason" type="text" class="form-control" placeholder="Reason for blacklisting">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label></label>
                                    <div class="controls">
                                        <div class="btn-group pull-right" role="group">
                                            <button class="btn btn-success" name="action" value="blacklist" type="submit">Blacklist</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="static/js/jquery.min.js"></script>
        <script src="static/js/bootstrap.min.js"></script>
    </body>
</html>
