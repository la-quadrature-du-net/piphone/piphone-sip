CREATE TABLE IF NOT EXISTS users(api PRIMARY KEY, token, admin);
CREATE TABLE IF NOT EXISTS calls (callid PRIMARY KEY, owner, caller, callee, history);
CREATE TABLE IF NOT EXISTS blacklist(id PRIMARY KEY, pattern, reason);
