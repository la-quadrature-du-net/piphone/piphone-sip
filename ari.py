import requests
import sqlite3

def ari_call(config, verbs, method, payload):
    '''
    Generate a call to the ARI. Config contains host, protocol, and API base.
    Return the result.json().
    '''
    try:
        base_url = config['protocol'] + '://' + config['server'] + '/' + config['api_base']
    except KeyError as e:
        raise e
    try:
        full_url = '/'.join([base_url, '/'.join(verbs)])
        if method.lower() == 'del':
            call = requests.delete
        else:
            call = getattr(requests, method.lower())
        payload['app'] = config['app']
        payload['api_key'] = config['key'] + ':' + config['password']
        result = call(full_url, data=payload)
        result.raise_for_status()
        if method.lower() == 'post':
            # We need to test if we have something in the object, to eventually return it
            try:
                return result.json()
            except:
                result = requests.get(full_url, data=payload)
        try:
            return result.json()
        except:
            return '{}'
    except:
        raise

class Bridge(object):
    '''
    This is a bridge object, we use it to manage various needed calls
    '''
    def __init__(self, config, name, mode='mixing'):
        self.name = name
        self.config = config
        self.mode = mode

    def status(self):
        '''
        Get the status of a specific bridge
        '''
        try:
            return ari_call(self.config, ['bridges', self.name], 'GET', {})
        except:
            raise

    def create(self):
        '''
        Create a bridge.
        '''
        try:
            return ari_call(self.config, ['bridges', self.name], 'POST', {'mode': self.mode})
        except:
            raise

    def startMoh(self, playback_class):
        '''
        Let's start a playback on this bridge
        '''
        try:
            return ari_call(self.config, ['bridges', self.name, 'moh'], 'POST', {'mohClass': playback_class})
        except:
            raise

    def addChannel(self, channel):
        '''
        We want to add a channel to the bridge
        '''
        try:
            return ari_call(self.config, ['bridges', self.name, 'addChannel'], 'POST', {'channel': channel})
        except:
            raise

    def removeChannel(self, channel):
        '''
        We want to remove a channel from the bridge
        '''
        try:
            return ari_call(self.config, ['bridges', self.name, 'removeChannel'], 'POST', {'channel': channel})
        except:
            raise

    def delete(self):
        '''
        Let's delete the bridge
        '''
        try:
            return ari_call(self.config, ['bridges', self.name], 'DEL', {})
        except:
            raise

class Channel(object):
    '''
    This class is used to regroup all the method to manage a Channel
    '''
    def __init__(self, config, name):
        self.config = config
        self.name = name

    def status(self):
        '''
        Get the status of a channel
        '''
        try:
            return ari_call(self.config, ['channels', self.name], 'GET', {})
        except:
            raise

    def originate(self, endpoint):
        '''
        Originate a call to endpoint.
        '''
        try:
            return ari_call(self.config, ['channels', self.name], 'POST', {'endpoint': endpoint})
        except:
            raise

    def playback(self, playback_id, media, lang='en_US'):
        '''
        Starts a playback on a Channels
        '''
        try:
            return ari_call(self.config, ['channels', self.name, 'play', playback_id], 'POST', {'media': media, 'lang': lang})
        except:
            raise

    def delete(self):
        '''
        Delete the channel (hangup)
        '''
        try:
            return ari_call(self.config, ['channels', self.name], 'DEL', {})
        except:
            raise

    def startMoh(self, moh_class):
        '''
        Start playing music on hold on the channel
        '''
        try:
            return ari_call(self.config, ['channels', self.name, 'moh'], 'POST', {'mohClass': moh_class})
        except:
            raise

    def stopMoh(self):
        '''
        Stop playing moh on the channel
        '''
        try:
            return ari_call(self.config, ['channels', self.name, 'moh'], 'DEL', {})
        except:
            raise

class Playback(object):
    '''
    This object is used to manage ARI calls for playbacks.
    '''
    def __init__(self, config, name, media, lang='en_US'):
        self.config = config
        self.name = name
        self.media = media
        self.lang = lang

    def stop(self):
        '''
        Let's stop a specific playback
        '''
        try:
             ari_call(self.config, ['playbacks', self.name], 'DEL', {})
        except:
            raise
