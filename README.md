# Piphone SIP Backend

This is the SIP Backend of the piphone part. Its purpose is to manage the calls
per se (issue them, connect them, put people on hold, and hanging them).

It's not intended as a full campaign manager, it just connect two phone calls
with each others, in fact it has no idea of identities and other related things.
It has no way to know if the phone number inserted are correct nor any other
things like this.

## Install
The simplest way to install the si-backend is to create a virtualenv and to
install the needed dependencies:

    $ pip install -r requirements.txt

You then need to create a sqlite database with the schema provided in the
database.sql file : 

    $ sqlite3 piphone.db < database.sql

## Config
The configuration file (in config.ini) must be edited and adapted to your
environment. Values are self explicit.

## SIP trunk
You need a SIP trunk available, with Asterisk 12+ and the Asterisk REST API
activated (ari). The address it listens to is used in the config file (in the
asterisk and websocket part of the configurationa).

We recommend you to use a specific account for the piphone on this asterisk
trunk.

## Running
Just run the script. You can specify the path for a config file using the -c
argument:

    $ python piphone.py -c </path/to/config/file.ini>

Kill it to stop it.

## API
### Authentification

The api available is an authenticated one, you need to add two parameters to
your query:

* token: a JWT token containing the value of your api_token, encrypted with
  your api_key
* api: the value of your api

Both the api and api_key value are extracted from the sqlite database, so you
need to generate them first.

You can generate a JWT token with the following calls in python for instance

    > import jwt
    > jwt.encode({'api': <your_api_token>}, '<your_api_key>'

There is similar libraries for most languages, see [jwti.io](https://jwt.io/)

### Calls
The api is a REST one

#### GET /calls/
List all calls made associated with this api_token

#### GET /calls/&lt;callid&gt;
Display one specific call.

#### POST /calls/
#### POST /calls/&lt;callid&gt;
*Params*:
* **caller**: Phone number (in international format: 00CCX… where CC is the
  country code and X… all the other digits)
* **callee**: Phone number (in international format: 00CCX… where CC is the
  country code and X… all the other digits)

Call the caller, great them, and wait for them to push 1 tehn redirect them
to a hold bridge, then call the callee, wait for them to pikcup the call and
connect the both of them together.

If the callid is given, the API will track this call using this specific ID.
Else, it will use a random uuid4 one.
